import os

all_glyphs_to_generate = []
generation_steps = []
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'map_files/generation_steps.txt'), 'r') as file:
    for line in file:
        line = line.strip()
        if line and line[0] != '#': # Skip empty lines and comment lines
            parts = line.split()
            generation_steps.append(parts)
            if (parts[0] == "IN"): all_glyphs_to_generate += parts[4]
            elif (parts[0] == "DUP"): all_glyphs_to_generate += parts[2]
            elif (parts[0] == "GC"): all_glyphs_to_generate += parts[1]

glyph_slots_by_name = {}
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'map_files/glyph_slots_by_name.txt'), 'r') as file:
    for line in file:
        line = line.strip()
        if line:  # Skip empty lines
            glyph_names = line.split()
            glyph_name = glyph_names[0]
            slot_number = int(glyph_names[1], 16)  # Convert hex string to integer
            glyph_slots_by_name[glyph_name] = slot_number

# The following composite glyphs contain overlapping references, and thus require fontforge to "unlink references and remove overlaps".
composite_glyphs_with_overlaps = {
			"Aring", "Ccedilla", "Eth", "Oslash", "ccedilla", "oslash", "Aogonek", "aogonek", "Dcroat", "dcroat", "Eogonek", "eogonek", "Hbar", "hbar", "Iogonek", "iogonek", "Lslash", "lslash",
			"Tcommaaccent", "tcommaaccent", "Tbar", "tbar", "Uogonek", "uogonek", "uni0180", "uni0189", "uni0197", "uni019F", "Ohorn", "ohorn", "Uhorn", "uhorn", "uni01B5", "uni01B6", "uni01BE",
			"uni01C2", "uni01E4", "uni01E5", "uni01EA", "uni01EB", "uni01EC", "uni01ED", "Aringacute", "Oslashacute", "oslashacute", "uni0228", "uni0229", "uni023A", "uni023B", "uni023C",
			"uni023D", "uni023E", "uni0243", "uni0244", "uni0246", "uni0247", "uni025F", "uni0275", "uni02A1", "uni02A2", "uni0492", "uni0493", "uni1E08", "uni1E09", "uni1E10", "uni1E11",
			"uni1E1C", "uni1E1D", "uni1E66", "uni1E67", "uni1E9C", "uni1E9D", "uni1EAE", "uni1EB0", "uni1EB2", "uni1EB4", "uni1EDA", "uni1EDB", "uni1EDC", "uni1EDD", "uni1EDE", "uni1EDF",
			"uni1EE0", "uni1EE1", "uni1EE2", "uni1EE3", "uni1EE8", "uni1EE9", "uni1EEA", "uni1EEB", "uni1EEC", "uni1EED", "uni1EEE", "uni1EEF", "uni1EF0", "uni1EF1", "dong",
			"ccedilla.pcap", "eth.pcap", "oslash.pcap", "aogonek.pcap", "eogonek.pcap", "hbar.pcap", "iogonek.pcap", "lslash.pcap", "scedilla.pcap", "tcedilla.pcap", "tbar.pcap",
			"uogonek.pcap", "obar.pcap", "ohorn.pcap", "uhorn.pcap", "clickalveolar.pcap", "uni01E5.pcap", "uni01EB.pcap", "uni01ED.pcap", "aringacute.pcap", "oslashacute.pcap", "uni0229.pcap",
			"uni1E09.pcap", "uni1E11.pcap", "uni1E1D.pcap", "uni1E67.pcap", "uni1EDB.pcap", "uni1EDD.pcap", "uni1EDF.pcap", "uni1EE1.pcap", "uni1EE3.pcap", "uni1EE9.pcap", "uni1EEB.pcap",
			"uni1EED.pcap", "uni1EEF.pcap", "uni1EF1.pcap"
		}

lc_uc_pcap_smcp_mappings = []
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'map_files/lc_uc_pcap_smcp_mappings.txt'), 'r') as file:
    for line in file:
        line = line.strip()
        if line:  # Skip empty lines
            glyph_names = line.split()
            lc_glyph_name = glyph_names[0]
            uc_glyph_name = glyph_names[1]
            pcap_glyph_name = glyph_names[2]
            smcp_glyph_name = glyph_names[3]
            lc_uc_pcap_smcp_mappings.append((lc_glyph_name, uc_glyph_name, pcap_glyph_name, smcp_glyph_name))

matching_width_glyph_groups = []
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'map_files/matching_width_glyphs.txt'), 'r') as file:
    for line in file:
        line = line.strip()
        if line:  # Skip empty lines
            glyph_names = line.split()
            matching_width_glyph_groups.append(glyph_names)
