import json
from interpolator import Interpolator
from interpolation_target import InterpolationTarget

def interpolate_font(target_json):
    target_data = json.loads(target_json)
    target = InterpolationTarget.from_dict(target_data)
    Interpolator.interpolate(target)

if __name__ == "__main__":
    import sys
    target_json = sys.argv[1]
    interpolate_font(target_json)
