import fontforge
import re
import os
from mappings import (
    glyph_slots_by_name,
    composite_glyphs_with_overlaps,
    lc_uc_pcap_smcp_mappings,
    matching_width_glyph_groups,
    generation_steps,
)

class FeatureGenerator:
    @classmethod
    def duplicate_font(cls, font):
        """Create a duplicate of the entire font."""
        temp_font_path = '/tmp/temp_font.sfd'
        font.save(temp_font_path)
        return fontforge.open(temp_font_path)

    @classmethod
    def copy_referenced_glyphs(cls, src_font, dst_font, glyph):
        """Recursively copy all referenced glyphs from src_font to dst_font."""
        if not glyph.references:
            return
        
        for ref in glyph.references:
            ref_name = ref[0]
            if ref_name not in dst_font:
                src_glyph = src_font[ref_name]
                cls.copy_referenced_glyphs(src_font, dst_font, src_glyph)
                dst_glyph = dst_font.createChar(glyph_slots_by_name.get(ref_name, None), ref_name)
                dst_glyph.foreground = src_glyph.foreground
                dst_glyph.width = src_glyph.width
                dst_glyph.references = src_glyph.references

    @classmethod
    def interpolate_contours(cls, contour_a, contour_b, proportion):
        new_contour = fontforge.contour()
        i_a, i_b = 0, 0
        is_bezier = False
        x1, y1, x2, y2, start_x, start_y = 0, 0, 0, 0, 0, 0
        has_stored_start = False
        
        while i_a < len(contour_a) and i_b < len(contour_b):
            point_a, point_b = contour_a[i_a], contour_b[i_b]
            
            if point_a.on_curve and point_b.on_curve:
                x = point_a.x * (1 - proportion) + point_b.x * proportion
                y = point_a.y * (1 - proportion) + point_b.y * proportion
                if not has_stored_start:
                    (start_x, start_y) = (x, y)
                    has_stored_start = True
                new_point = fontforge.point(x, y, on_curve=1, type=min(point_a.type, point_b.type))
                if is_bezier:
                    new_contour.cubicTo((x1, y1), (x2, y2), (x, y))
                    is_bezier = False
                else:
                    new_contour.insertPoint(new_point)
                i_a += 1
                i_b += 1
            elif point_a.on_curve:
                # Interpolate the first control point
                prev_a = contour_a[i_a - 1] if i_a > 0 else point_a
                x1 = prev_a.x * (1 - proportion) + point_b.x * proportion
                y1 = prev_a.y * (1 - proportion) + point_b.y * proportion
                
                # Interpolate the second control point
                next_b = contour_b[i_b + 1] if i_b + 1 < len(contour_b) else point_b
                x2 = point_a.x * (1 - proportion) + next_b.x * proportion
                y2 = point_a.y * (1 - proportion) + next_b.y * proportion
                
                i_b += 2
                is_bezier = True
            elif point_b.on_curve:
                # Interpolate the first control point
                prev_b = contour_b[i_b - 1] if i_b > 0 else point_b
                x1 = point_a.x * (1 - proportion) + prev_b.x * proportion
                y1 = point_a.y * (1 - proportion) + prev_b.y * proportion
                
                # Interpolate the second control point
                next_a = contour_a[i_a + 1] if i_a + 1 < len(contour_a) else point_a
                x2 = next_a.x * (1 - proportion) + point_b.x * proportion
                y2 = next_a.y * (1 - proportion) + point_b.y * proportion
                
                i_a += 2
                is_bezier = True
            else:  # Both are curves
                # Interpolate this and the next point for both i_a and i_b
                x1 = point_a.x * (1 - proportion) + point_b.x * proportion
                y1 = point_a.y * (1 - proportion) + point_b.y * proportion
                
                next_a = contour_a[i_a + 1] if i_a + 1 < len(contour_a) else point_a
                next_b = contour_b[i_b + 1] if i_b + 1 < len(contour_b) else point_b
                x2 = next_a.x * (1 - proportion) + next_b.x * proportion
                y2 = next_a.y * (1 - proportion) + next_b.y * proportion
                
                i_a += 2
                i_b += 2
                is_bezier = True
        
        # Close the contour
        if is_bezier:
            new_contour.cubicTo((x1,y1),(x2,y2), (start_x, start_y))
        new_contour.closed = True
        return new_contour

    @classmethod
    def match_and_interpolate_contours(cls, glyph_a, glyph_b, proportion):
        new_layer = fontforge.layer()

        # Create lists of unused contours
        contours_a = list(glyph_a.layers["Fore"])
        unused_contours_a = contours_a[:]
        unused_contours_b = list(glyph_b.layers["Fore"])

        # Function to count on-curve points in a contour
        def count_on_curve_points(contour):
            return sum(1 for point in contour if point.on_curve)

        # Match contours based on the number of on-curve points
        matched_contours = []
        for contour_a in contours_a:
            count_a = count_on_curve_points(contour_a)
            for contour_b in unused_contours_b:
                if count_on_curve_points(contour_b) == count_a:
                    matched_contours.append((contour_a, contour_b))
                    unused_contours_a.remove(contour_a)
                    unused_contours_b.remove(contour_b)
                    break

        # Interpolate matched contours
        for contour_a, contour_b in matched_contours:
            new_contour = cls.interpolate_contours(contour_a, contour_b, proportion)
            new_layer += new_contour

        # Add unmatched contours as-is
        # for contour in unused_contours_a:
        #     new_layer += contour
        # for contour in unused_contours_b:
        #     new_layer += contour

        return new_layer

    @classmethod
    def match_and_interpolate_refs(cls, font, new_glyph, glyph_a, glyph_b, proportion, suppress_warnings = False):
        refs_a = list(reversed(glyph_a.references)) # reversing because fontforge inserts refs at start of list, not end
        refs_b = list(reversed(glyph_b.references)) # reversing because fontforge inserts refs at start of list, not end

        # Initialize lists to keep track of unmatched references
        unused_refs_a = refs_a[:]
        unused_refs_b = refs_b[:]

        for ref_a in refs_a:
            ref_name_a = ref_a[0]

            # Try to find a matching reference in refs_b
            for ref_b in unused_refs_b:
                ref_name_b = ref_b[0]

                if ref_name_a == ref_name_b:
                    # Extract transformation matrices
                    matrix_a = ref_a[1]
                    matrix_b = ref_b[1]

                    # Interpolate the 6-element transformation matrix
                    interpolated_matrix = [
                        matrix_a[i] * (1 - proportion) + matrix_b[i] * proportion
                        for i in range(6)
                    ]

                    # Add the interpolated reference to the new glyph
                    if ref_name_a in font:
                        new_glyph.addReference(ref_name_a, interpolated_matrix)
                    else:
                        if not suppress_warnings: print(f"Can't add reference to {ref_name_a}: doesn't exist in font!")

                    # Remove matched references from the unused lists
                    unused_refs_a.remove(ref_a)
                    unused_refs_b.remove(ref_b)
                    break

        for ref_name, ref in unused_refs_a:
            if ref_name in font:
                if not suppress_warnings: print(f"Unmatched reference to {ref_name} in {glyph_a}")
                # new_glyph.addReference(ref_name, ref)
            else:
                if not suppress_warnings: print(f"Can't add reference (a) to {ref_name} from {glyph_a}: doesn't exist in the font")
        for ref_name, ref in unused_refs_b:
            if ref_name in font:
                if not suppress_warnings: print(f"Unmatched reference to {ref_name} in {glyph_b}")
                # new_glyph.addReference(ref_name, ref)
            else:
                if not suppress_warnings: print(f"Can't add reference (b) to {ref_name} from {glyph_b}: doesn't exist in the font")

    @classmethod
    def interpolate_glyphs(cls, font, glyph_a, glyph_b, proportion, slot_index, glyph_name, suppress_warnings = False):
        """Interpolate glyphs via custom method"""
        # Remove the existing glyph if it exists
        if glyph_name in font:
            font.removeGlyph(font[glyph_name])
        
        # Create a new glyph at the desired slot with the specified name
        new_glyph = font.createChar(slot_index, glyph_name)

        new_layer = cls.match_and_interpolate_contours(glyph_a, glyph_b, proportion)
        new_glyph.layers["Fore"] = new_layer
        
        # Interpolating anchor points, matching by both class name and type
        anchors_a = {(anchor[0], anchor[1]): anchor for anchor in glyph_a.anchorPoints}
        anchors_b = {(anchor[0], anchor[1]): anchor for anchor in glyph_b.anchorPoints}
        # Find common anchors by matching both class name and type
        common_anchors = set(anchors_a.keys()).intersection(anchors_b.keys())

        for anchor_key in common_anchors:
            # Extract the anchor positions for interpolation
            x_a, y_a = float(anchors_a[anchor_key][2]), float(anchors_a[anchor_key][3])
            x_b, y_b = float(anchors_b[anchor_key][2]), float(anchors_b[anchor_key][3])
            x_c = x_a * (1 - proportion) + x_b * proportion
            y_c = y_a * (1 - proportion) + y_b * proportion
            
            # Add the interpolated anchor to the new glyph
            anchor_class_name, anchor_type = anchor_key
            new_glyph.addAnchorPoint(anchor_class_name, anchor_type, x_c, y_c)

        # Interpolating linked references
        cls.match_and_interpolate_refs(font, new_glyph, glyph_a, glyph_b, proportion, suppress_warnings)

        # Copying over additional properties if necessary
        new_glyph.width = round(glyph_a.width * (1 - proportion) + glyph_b.width * proportion)
        new_glyph.vwidth = round(glyph_a.vwidth * (1 - proportion) + glyph_b.vwidth * proportion)
        new_glyph.unlinkRmOvrlpSave = glyph_a.unlinkRmOvrlpSave or glyph_b.unlinkRmOvrlpSave
        
        return new_glyph

    @classmethod
    def _get_glyph_path(cls, folder_path, glyph_name):
        # Prefix capital letters with underscore exept in glyph names like "uni01EB" (lowercase "uni" and four hex digits)
        if re.match(r'^uni[0-9A-F]{4}$', glyph_name):
            transformed_name = glyph_name
        else:
            transformed_name = ''.join(f'_{char}' if char.isupper() else char for char in glyph_name)
        
        # Construct the file path by appending the '.glyph' extension
        file_name = f"{transformed_name}.glyph"
        file_path = os.path.join(folder_path, file_name)
        
        return file_path

    @classmethod
    def _read_anchors_from_glyph_file(cls, path):
        anchors = []
        with open(path, 'r') as file:
            for line in file:
                if line.startswith("AnchorPoint:"):
                    parts = line.split()
                    anchor_class_name = parts[1].strip('"')
                    x = float(parts[2])
                    y = float(parts[3])
                    anchor_type = parts[4].replace("basechar", "base")  # Normalize basechar to base
                    ligature_index = int(parts[5]) if anchor_type == "ligature" else None
                    
                    anchor_tuple = (anchor_class_name, anchor_type, x, y)
                    if ligature_index is not None:
                        anchor_tuple += (ligature_index,)
                    
                    anchors.append(anchor_tuple)
        
        return anchors

    @classmethod
    def _generate_composite_glyph(cls, font, glyph_name, base_name, mark_name, anchor_name):
        # Check if both the base and mark glyphs exist in the font
        if base_name not in font or mark_name not in font:
            print(f"skipping composite generation of {glyph_name} as {base_name} or {mark_name} not in font")
            return

        base_glyph = font[base_name]
        mark_glyph = font[mark_name]

        # Find the base and mark anchors with the specified name
        # base_anchors = cls._read_anchors_from_glyph_file(cls._get_glyph_path(font_folder_path, base_name)) # FontForge has bugs reading anchors
        # mark_anchors = cls._read_anchors_from_glyph_file(cls._get_glyph_path(font_folder_path, mark_name)) # FontForge has bugs reading anchors
        base_anchors = base_glyph.anchorPoints
        mark_anchors = mark_glyph.anchorPoints
        base_anchor = next((a for a in base_anchors if a[0] == anchor_name and a[1] == 'base'), None)
        mark_anchor = next((a for a in mark_anchors if a[0] == anchor_name and a[1] == 'mark'), None)

        if base_anchor is None:
            print(f"Skipping composite generation of {glyph_name} because base glyph {base_name} is missing {anchor_name} anchor.")
            return
        if mark_anchor is None:
            print(f"Skipping composite generation of {glyph_name} because mark glyph {mark_name} is missing {anchor_name} anchor.")
            return

        # Remove the existing glyph if it exists
        if glyph_name in font:
            font.removeGlyph(font[glyph_name])

        # Create the composite glyph
        composite_glyph = font.createChar(glyph_slots_by_name[glyph_name], glyph_name)
        composite_glyph.width = base_glyph.width

        # Calculate the translation needed for the mark glyph
        dx = base_anchor[2] - mark_anchor[2]
        dy = base_anchor[3] - mark_anchor[3]

        # Add the mark glyph as a reference with the calculated translation
        composite_glyph.addReference(mark_name, (1.0, 0.0, 0.0, 1.0, dx, dy))

        # Add the base glyph as a reference without translation
        composite_glyph.addReference(base_name, (1.0, 0.0, 0.0, 1.0, 0.0, 0.0))
        composite_glyph.useRefsMetrics(base_name) # sets a flag on the reference to bind the advance width to that of the reference.

        # Copy anchor points from the base glyph, excluding the one used for the mark
        for anchor in base_glyph.anchorPoints:
            if anchor[0] != anchor_name:
                composite_glyph.addAnchorPoint(anchor[0], anchor[1], anchor[2], anchor[3])

        # Handle the UnlinkRmOvrlpSave property based on conditions
        composite_glyph.unlinkRmOvrlpSave = (glyph_name in composite_glyphs_with_overlaps or
                                                base_glyph.unlinkRmOvrlpSave or
                                                mark_glyph.unlinkRmOvrlpSave)

        #print(f"Created composite glyph {glyph_name} from {base_name} and {mark_name}")
        
    @classmethod
    def generate_pcap_and_smcp_lookups(cls, font):
        if not "c2pc" in font.gsub_lookups:
            font.addLookup("c2pc", "gsub_single", None, (("c2pc", (("latn",("dflt")),)),))
            font.addLookupSubtable("c2pc", "c2pc-1")
        if not "c2sc" in font.gsub_lookups:
            font.addLookup("c2sc", "gsub_single", None, (("c2sc", (("latn",("dflt")),)),))
            font.addLookupSubtable("c2sc", "c2sc-1")
        if not "pcap" in font.gsub_lookups:
            font.addLookup("pcap", "gsub_single", None, (("pcap", (("latn",("dflt")),)),))
            font.addLookupSubtable("pcap", "pcap-1")
        if not "smcp" in font.gsub_lookups:
            font.addLookup("smcp", "gsub_single", None, (("smcp", (("latn",("dflt")),)),))
            font.addLookupSubtable("smcp", "smcp-1")

        for lc_name, uc_name, pc_name, sc_name in lc_uc_pcap_smcp_mappings:
            if uc_name != "?" and pc_name != "?":
                cls.create_substitution(font, "c2pc-1", uc_name, pc_name)
            if uc_name != "?" and sc_name != "?":
                cls.create_substitution(font, "c2sc-1", uc_name, sc_name)
            if lc_name != "?" and pc_name != "?":
                cls.create_substitution(font, "pcap-1", lc_name, pc_name)
            if lc_name != "?" and sc_name != "?":
                cls.create_substitution(font, "smcp-1", lc_name, sc_name)

    @classmethod
    def create_substitution(cls, font, lookup_name, from_name, to_name):
        if from_name in font and to_name in font:
            from_glyph = font[from_name]
            # Replace or add a substitution in the lookup table
            from_glyph.addPosSub(lookup_name, to_name)
        
    @classmethod
    def match_widths_for_glyphs_tied_to_base_glyph_width(cls, font):
        print('TODO: match_widths_for_glyphs_tied_to_base_glyph_width')
        # TODO FontForge doesn't provide any way to read the use_my_metrics flag on references,
        # which indicates if the glyph should be tied to the width of the referenced glyph.
        # Occasionally a glyph can have incorrect width when this flag is set, and
        # this method should correct that.
        # Perhaps we need to open the .glyph file and parse that directly?
        
    @classmethod
    def match_widths_for_other_glyphs(cls, font):
        for glyph_group in matching_width_glyph_groups:
            if not glyph_group[0] in font: continue
            width = font[glyph_group[0]].width
            for glyph_name in glyph_group[1:]:
                if not glyph_name in font: continue
                font[glyph_name].width = width
        # print('match_widths_for_other_glyphs')

    @classmethod
    def _do_generation_steps(cls, font):
        for step in generation_steps:
            command, *parts = step
            if command == "IN": # Interpolate
                proportion = float(parts[0])
                [glyph_a, glyph_b, new_glyph_name] = parts[1:]
                slot_index = glyph_slots_by_name.get(new_glyph_name, None)
                if glyph_a in font and glyph_b in font and slot_index is not None:
                    # Interpolate and create the small numeral glyph
                    cls.interpolate_glyphs(font, font[glyph_a], font[glyph_b], proportion, slot_index, new_glyph_name)
            elif command == "DUP": # Duplicate
                [base_name, new_glyph_name] = parts
                slot_index = glyph_slots_by_name.get(new_glyph_name, None)
                if slot_index is not None and base_name in font:
                    base_glyph = font[base_name]
                    # Remove the existing glyph if it exists
                    if new_glyph_name in font:
                        font.removeGlyph(font[new_glyph_name])
                    # Create a new glyph at the desired slot with the specified name
                    new_glyph = font.createChar(slot_index, new_glyph_name)
                    # Set the width and duplicate the foreground reference
                    new_glyph.width = base_glyph.width
                    new_glyph.addReference(base_name, (1.0, 0.0, 0.0, 1.0, 0.0, 0.0))
                    new_glyph.useRefsMetrics(base_name) # sets a flag on the reference to bind the advance width to that of the reference.
                    # Copy anchor points from the base glyph
                    for anchor in base_glyph.anchorPoints:
                        new_glyph.addAnchorPoint(anchor[0], anchor[1], anchor[2], anchor[3])
            elif command == "GC": # Generate composite
                [new_glyph_name, base_name, mark_name, anchor_name] = parts
                slot_index = glyph_slots_by_name.get(new_glyph_name, None)
                if slot_index is not None and base_name in font and mark_name in font:
                    cls._generate_composite_glyph(font, new_glyph_name, base_name, mark_name, anchor_name)
            elif command == "GCF": # Generate composite as a fallback (only if the glyph could not be interpolated)
                [new_glyph_name, base_name, mark_name, anchor_name] = parts
                slot_index = glyph_slots_by_name.get(new_glyph_name, None)
                if (
                    slot_index is not None
                    and base_name in font
                    and mark_name in font
                    and (not new_glyph_name in font or len(font[new_glyph_name].references) < 2)
                ):
                    cls._generate_composite_glyph(font, new_glyph_name, base_name, mark_name, anchor_name)

    @classmethod
    def generate_derived_features(cls, font):
        cls._do_generation_steps(font)
        cls.generate_pcap_and_smcp_lookups(font)
        cls.match_widths_for_glyphs_tied_to_base_glyph_width(font)
        cls.match_widths_for_other_glyphs(font)
