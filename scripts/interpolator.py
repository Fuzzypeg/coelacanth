import os
import fontforge
from feature_generator import FeatureGenerator
from notices import copyright_notice, font_comment, font_version
from mappings import all_glyphs_to_generate

class Interpolator:

    @staticmethod
    def _reorder_paths(glyph):
        """Reorder the paths on the Fore layer by number of on-curve points."""
        if glyph.foreground.isEmpty():
            return
        
        # Extract contours
        contours = [contour for contour in glyph.foreground]
        
        # Sort contours by the number of on-curve points
        sorted_contours = sorted(contours, key=lambda contour: len([pt for pt in contour if pt.on_curve]))

        # Create a new layer and re-add the sorted contours
        new_layer = fontforge.layer()
        for contour in sorted_contours:
            new_layer += contour
        
        # Assign the new layer to the foreground
        glyph.layers["Fore"] = new_layer


    @staticmethod
    def _reorder_references(glyph):
        """Reorder the references in alphabetic order of the glyph-name they refer to."""
        references = glyph.references
        sorted_references = sorted(references, key=lambda ref: ref[0])

        # Clear references and re-add them in sorted order
        glyph.references = []
        for ref in reversed(sorted_references): # reversing because fontforge inserts at start of list, not at end
            glyph.addReference(ref[0], ref[1])

    @staticmethod
    def _cleanup_font(font):
        """Reorder paths and references for each glyph in the font."""
        for glyph in font.glyphs():
            Interpolator._reorder_paths(glyph)
            Interpolator._reorder_references(glyph)

    @staticmethod
    def _interpolate_anchors(glyph_a, glyph_b, glyph_c, proportion):
        """Interpolate anchor points for each anchor that exists in both glyph_a and glyph_b."""
        if not glyph_a or not glyph_b:
            return
        
        # Extract anchors from glyph_a and glyph_b
        anchors_a = {(anchor[0], anchor[1]): anchor for anchor in glyph_a.anchorPoints}
        anchors_b = {(anchor[0], anchor[1]): anchor for anchor in glyph_b.anchorPoints}
        
        # Find common anchors by name and type
        common_anchors = set(anchors_a.keys()).intersection(anchors_b.keys())
        
        for anchor_name, anchor_type in common_anchors:
            x_a, y_a = float(anchors_a[(anchor_name, anchor_type)][2]), float(anchors_a[(anchor_name, anchor_type)][3])
            x_b, y_b = float(anchors_b[(anchor_name, anchor_type)][2]), float(anchors_b[(anchor_name, anchor_type)][3])
            x_c = x_a * (1 - proportion) + x_b * proportion
            y_c = y_a * (1 - proportion) + y_b * proportion
            glyph_c.addAnchorPoint(anchor_name, anchor_type, x_c, y_c)

    @classmethod
    def _clean_up_glyph_interpolation(cls, glyph_a, glyph_b, glyph_c, proportion):
        if (glyph_a and glyph_a.unlinkRmOvrlpSave) or (glyph_b and glyph_b.unlinkRmOvrlpSave):
            glyph_c.unlinkRmOvrlpSave = True
        cls._interpolate_anchors(glyph_a, glyph_b, glyph_c, proportion)

    @classmethod
    def _interpolate_ps_private(cls, font_a, font_b, font_c, proportion):
        """
        Interpolates values for the PostScript private dictionary of font_c 
        based on the private dictionaries of font_a and font_b using the specified proportion.
        """
        private_a = font_a.private
        private_b = font_b.private
        private_c = font_c.private

        keys_of_interest = ['StdHW', 'StemSnapH', 'StdVW', 'StemSnapV', 'BlueValues', 'OtherBlues']

        for key in keys_of_interest:
            if key in private_a and key in private_b:
                # Get the values from both private dictionaries
                values_a = private_a[key]
                values_b = private_b[key]

                # Interpolate matching values
                interpolated_values = [
                    round(values_a[i] * (1 - proportion) + values_b[i] * proportion)
                    for i in range(min(len(values_a), len(values_b)))
                ]

                # Assign the interpolated values to the private dictionary of font_c
                private_c[key] = interpolated_values

    @classmethod
    def _getSfntRevision(cls):
        try:
            parts = font_version.split(".")
            if len(parts) != 3: raise ValueError(f"Invalid version string: {version_str}")
            a, b, c = map(int, parts)
            return a + b * 0.01 + c * 0.0001
        except ValueError as e:
            print(f"Error converting version string to float: {e}")
            return None

    @classmethod
    def _initialise_target_font(cls, target, font_a, font_b):
        # if target.base_a_path == target.base_b_path:
        #     # Duplicate font_a if font_a and font_b are the same
        #     font_c = fontforge.open(target.base_a_path)
        # else:
        #     # Perform interpolation
        #     font_c = font_a.interpolateFonts(target.proportion, target.base_b_path)

        font_c = fontforge.font()
        font_c.encoding = 'UnicodeBMP'

        # Set additional properties for font_c
        font_c.fontname = target.target_short_name.replace(" ", "")
        font_c.familyname = target.family
        font_c.fullname = target.target_long_name
        font_c.version = font_version
        font_c.sfntRevision = cls._getSfntRevision()
        font_c.weight = target.weight_name
        font_c.os2_weight = int(target.os2_weight_code)
        font_c.os2_typoascent = round(font_a.os2_typoascent * (1 - target.proportion) + font_b.os2_typoascent * target.proportion)
        font_c.os2_typoascent_add = font_a.os2_typoascent_add
        font_c.os2_typodescent = round(font_a.os2_typodescent * (1 - target.proportion) + font_b.os2_typodescent * target.proportion)
        font_c.os2_typodescent_add = font_a.os2_typodescent_add
        font_c.os2_winascent = round(font_a.os2_winascent * (1 - target.proportion) + font_b.os2_winascent * target.proportion)
        font_c.os2_winascent_add = font_a.os2_winascent_add
        font_c.os2_windescent = round(font_a.os2_windescent * (1 - target.proportion) + font_b.os2_windescent * target.proportion)
        font_c.os2_windescent_add = font_a.os2_windescent_add
        font_c.hhea_ascent = round(font_a.hhea_ascent * (1 - target.proportion) + font_b.hhea_ascent * target.proportion)
        font_c.hhea_ascent_add = font_a.hhea_ascent_add
        font_c.hhea_descent = round(font_a.hhea_descent * (1 - target.proportion) + font_b.hhea_descent * target.proportion)
        font_c.hhea_descent_add = font_a.hhea_descent_add
        font_c.os2_typolinegap = round(font_a.os2_typolinegap * (1 - target.proportion) + font_b.os2_typolinegap * target.proportion)
        font_c.hhea_linegap = round(font_a.hhea_linegap * (1 - target.proportion) + font_b.hhea_linegap * target.proportion)
        font_c.os2_vendor = font_a.os2_vendor
        font_c.os2_family_class = font_a.os2_family_class

        font_c.design_size = target.size_points_and_range[0]
        font_c.size_feature = (
            target.size_points_and_range[0],
            target.size_points_and_range[1],
            target.size_points_and_range[2],
            target.size_style_id,
            (("English (US)", "Regular"),),
        )

        font_c.copyright = copyright_notice
        font_c.comment = font_comment
        font_c.version = font_version

        font_c.ascent = round(font_a.ascent * (1 - target.proportion) + font_b.ascent * target.proportion)
        font_c.descent = round(font_a.descent * (1 - target.proportion) + font_b.descent * target.proportion)
        # TODO interpolate tangents
        font_c.italicangle = round(font_a.italicangle * (1 - target.proportion) + font_b.italicangle * target.proportion)
        font_c.upos = round(font_a.upos * (1 - target.proportion) + font_b.upos * target.proportion)
        font_c.uwidth = round(font_a.uwidth * (1 - target.proportion) + font_b.uwidth * target.proportion)

        return font_c

    @classmethod
    def _copy_lookups(cls, font_a, font_c):
        def recopy_lookup_features(source_font, target_font, lookup_name):
            """FontForge's importLookups method erroneously only copies the first feature for the lookup. So we recopy all features"""
            source_info = source_font.getLookupInfo(lookup_name)
            source_type, source_flags, source_feature_script_langs = source_info
            # Set or update the feature list for the lookup in the target font
            if lookup_name in target_font.gsub_lookups or lookup_name in target_font.gpos_lookups:
                # print(f"Updating feature list for existing lookup {lookup_name} in {target_font.fontname}")
                target_font.lookupSetFeatureList(lookup_name, source_feature_script_langs)

        for lookup_name in font_a.gsub_lookups:
            if not lookup_name in font_c.gsub_lookups:
                font_c.importLookups(font_a, lookup_name)
                recopy_lookup_features(font_a, font_c, lookup_name)
        for lookup_name in font_a.gpos_lookups:
            if not lookup_name in font_c.gpos_lookups:
                # Note: This is copying the anchor points a second time!
                font_c.importLookups(font_a, lookup_name)
                recopy_lookup_features(font_a, font_c, lookup_name)

        # Remove duplicate anchor points from each glyph
        for glyph_name in font_c:
            glyph = font_c[glyph_name]
            anchor_points = glyph.anchorPoints
            unique_anchors = {}

            # Iterate over each anchor point and only keep distinct ones
            for anchor in anchor_points:
                class_name = anchor[0]
                anchor_type = anchor[1]
                coordinates = (anchor[2], anchor[3])

                # Keep only the last occurrence of this anchor, in case of duplication.
                # The spurious anchors copied with GPOS lookups will be first in the list.
                unique_anchors[(class_name, anchor_type)] = anchor

            # Clear existing anchor points
            glyph.anchorPoints = []

            # Re-add distinct anchor points
            for anchor in unique_anchors.values():
                if len(anchor) == 5:  # Ligature anchor point with ligature-index
                    glyph.addAnchorPoint(anchor[0], anchor[1], anchor[2], anchor[3], anchor[4])
                else:
                    glyph.addAnchorPoint(anchor[0], anchor[1], anchor[2], anchor[3])
            

    @classmethod
    def _interpolate_all_glyphs(cls, font_a, font_b, target_font, proportion, should_supplement_if_missing = False):
        glyphs_to_create = list(font_a)

        # Interpolate composite glyphs only after their dependencies have been interpolated
        while glyphs_to_create:
            progress_made = False
            for glyph_name in glyphs_to_create[:]:
                glyph_a = font_a[glyph_name]
                glyph_b = font_b[glyph_name] if glyph_name in font_b else None

                unresolved_refs = [
                    ref[0] for ref in glyph_a.references
                    if ref[0] in glyphs_to_create
                ]
                if unresolved_refs: continue

                # Interpolate the glyph if it exists in both font_a and font_b
                if glyph_b:
                    FeatureGenerator.interpolate_glyphs(
                        target_font,
                        glyph_a,
                        glyph_b,
                        proportion,
                        glyph_a.unicode,
                        glyph_name,
                        glyph_name in all_glyphs_to_generate,
                    )
                    glyphs_to_create.remove(glyph_name)
                    progress_made = True
                elif not glyph_name in all_glyphs_to_generate:
                    print(f"Glyph {glyph_name} missing in {font_b.fontname}: supplementing from {font_a.fontname}")
                    new_glyph = target_font.createChar(glyph_a.unicode, glyph_name)
                    new_glyph.width = glyph_a.width
                    new_layer = fontforge.layer()
                    for contour in glyph_a.layers["Fore"]:
                        new_layer += contour
                    new_glyph.foreground = new_layer
                    new_glyph.references = glyph_a.references  # Copy references if any
                    for anchor_name, anchor_type, x, y in glyph_a.anchorPoints:
                        new_glyph.addAnchorPoint(anchor_name, anchor_type, x, y)  # Copy anchor points
                    new_glyph.unlinkRmOvrlpSave = glyph_a.unlinkRmOvrlpSave
                    glyphs_to_create.remove(glyph_name)
                    progress_made = True
                
            if not progress_made:
                print(f"Possible cyclic references! Skipping remaining glyph interpolations.")
                break

    @classmethod
    def _round_contour_points_to_int(cls, font):
        for glyph_name in font:
            glyph = font[glyph_name]
            new_layer = fontforge.layer()
            for contour in glyph.layers["Fore"]:
                contour.round()
                new_layer += contour
            glyph.layers["Fore"] = new_layer  # Replace the layer with the updated one

    @classmethod
    def interpolate(cls, target):
        print(f"========\n{target.target_short_name}\n     {os.path.basename(target.base_a_path)}\n  => {os.path.basename(target.base_b_path)}\n     {target.proportion}")
        if not os.path.exists(target.target_path):
            os.makedirs(target.target_path)
        
        # Open the base fonts
        font_a = fontforge.open(target.base_a_path)
        font_b = fontforge.open(target.base_b_path)

        # Perform cleanup before interpolation
        cls._cleanup_font(font_a)
        cls._cleanup_font(font_b)

        # print("Get basic interpolation")
        font_c = cls._initialise_target_font(target, font_a, font_b)
        cls._interpolate_all_glyphs(font_a, font_b, font_c, target.proportion, True)
        cls._copy_lookups(font_a, font_c)
        cls._interpolate_ps_private(font_a, font_b, font_c, target.proportion)
        
        # print(f"Generating derived features for {target.target_short_name}")
        FeatureGenerator.generate_derived_features(font_c)

        if not target.is_mock:
            if not os.path.exists(target.target_path):
                os.makedirs(target.target_path)
            font_c.save(target.target_path)
            print(f"Saved interpolated font to {target.target_path}")

            cls._round_contour_points_to_int(font_c)
            # Save the rounded font as an OTF file
            otf_path = target.target_path.replace(".sfdir", ".otf")
            font_c.generate(otf_path)
            print(f"Generated OTF font: {otf_path}")

        # print(f"Performed spacing and kerning for {target.target_short_name}")

