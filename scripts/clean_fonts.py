import os
import re
import fontforge
import shutil
import tempfile

def deselect_glyph_elements(font):
    """Ensure no points, anchors, or references are selected in any glyph and validate each glyph."""
    for glyph in font.glyphs():
        # Clear all points selection in the glyph
        for contour in glyph.foreground:
            for point in contour:
                point.selected = False

        # Deselect all anchor points
        anchor_points = glyph.anchorPointsWithSel
        if anchor_points:
            deselected_anchors = []
            for anchor in anchor_points:
                if len(anchor) == 5:
                    deselected_anchors.append((anchor[0], anchor[1], anchor[2], anchor[3], False))
                elif len(anchor) == 6:
                    deselected_anchors.append((anchor[0], anchor[1], anchor[2], anchor[3], False, anchor[5]))

            glyph.anchorPoints = deselected_anchors

        # Deselect all references
        if glyph.references:
            original_references = glyph.references
            glyph.references = [] # Clear existing references

            # Re-add references without selection
            for ref in reversed(original_references): # Reversing since FontForge adds to front of list
                if len(ref) >= 2:
                    ref_name, matrix = ref[0], ref[1]
                    glyph.addReference(ref_name, matrix)

    # Ensure no glyphs are selected in the font
    font.selection.none()

def remove_auto_hints(font):
    """Remove all automatically generated hints from every glyph in the font."""
    for glyph in font.glyphs():
        # Clear all horizontal and vertical hints
        glyph.hhints = []
        glyph.vhints = []

def clean_features_from_glyph_file(filepath):
    try:
        # Read the contents of the file
        with open(filepath, 'r') as file:
            lines = file.readlines()

        cleaned_lines = []
        is_in_spline_set = False  # Track if we are inside a SplineSet section
        spline_regex = re.compile(r'^(.*? [mcl] )(\d+)$')  # Matches flag value at end of spline line

        for line in lines:
            if line.startswith('Validated:'):
                continue  # Skip validated lines

            # Replace ' S' with ' N' in Refer: lines
            line = re.sub(r'(Refer: .*?) S(\s|$)', r'\1 N\2', line)

            if line.strip() == "SplineSet":
                is_in_spline_set = True
            elif line.strip() == "EndSplineSet":
                is_in_spline_set = False

            # If inside SplineSet, modify point flags
            if is_in_spline_set:
                match = spline_regex.match(line)
                if match:
                    start_of_line = match.group(1)  # Keep everything before the flags
                    flag_value = int(match.group(2))  # Extract numeric flags
                    new_flag_value = flag_value & ~4  # Clear the 3rd bit (2^2)
                    line = f"{start_of_line}{new_flag_value}\n"  # Reconstruct line

            cleaned_lines.append(line)

        # Write back the modified contents to the file
        with open(filepath, 'w') as file:
            file.writelines(cleaned_lines)

    except Exception as e:
        print(f"Error processing {filepath}: {e}")

def cleanse_font_props(sfdir_path):
    props_file_path = os.path.join(sfdir_path, 'font.props')
    
    try:
        with open(props_file_path, 'r') as file:
            lines = file.readlines()
        modified_lines = []
        for line in lines:
            if line.startswith('ModificationTime:'):
                modified_lines.append('ModificationTime: 1704020400\n')
            elif line.startswith('WinInfo:'):
                modified_lines.append('WinInfo: 25 25 10\n')
            elif line.startswith('DisplaySize:'):
                modified_lines.append('DisplaySize: -72\n')
            else:
                modified_lines.append(line)
        with open(props_file_path, 'w') as file:
            file.writelines(modified_lines)
    except Exception as e:
        print(f"Error processing {props_file_path}: {e}")

def clean_features_from_glyph_files(directory):
    """Process all .glyph files within a .sfdir font directory."""
    try:
        for root, _, files in os.walk(directory):
            # Only process the immediate .sfdir directory, not nested ones
            if root == directory:
                for filename in files:
                    if filename.endswith('.glyph'):
                        clean_features_from_glyph_file(os.path.join(root, filename))
    except Exception as e:
        print(f"Error processing directory {directory}: {e}")


def process_sfdir_directory(directory):
    """Open a .sfdir font directory, process it, and save changes."""
    try:
        # Create a temporary directory
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_sfdir = os.path.join(temp_dir, os.path.basename(directory))
            shutil.copytree(directory, temp_sfdir)
            font = fontforge.open(temp_sfdir)

            deselect_glyph_elements(font)
            remove_auto_hints(font)

            # Attempt to save to a new directory to avoid overwriting issues
            new_temp_sfdir = os.path.join(temp_dir, 'processed_' + os.path.basename(directory))
            font.save(new_temp_sfdir)

            clean_features_from_glyph_files(new_temp_sfdir)
            cleanse_font_props(new_temp_sfdir)

            # Remove the original and replace it with the cleaned version
            shutil.rmtree(directory)
            shutil.move(new_temp_sfdir, directory)

        print(f"Processed and saved: {directory}")
    except Exception as e:
        print(f"Error processing {directory}: {e}")

def main():
    # Current working directory
    cwd = os.getcwd()

    # Iterate over directories directly under the current working directory
    for entry in os.listdir(cwd):
        full_path = os.path.join(cwd, entry)
        if os.path.isdir(full_path) and entry.endswith('.sfdir'):
            process_sfdir_directory(full_path)

if __name__ == "__main__":
    main()
