import os

# Define the root folder for the fonts
script_dir = os.path.dirname(os.path.abspath(__file__))
font_root_folder = os.path.normpath(os.path.abspath(os.path.join(script_dir, '../')))
interpolations_folder = os.path.join(font_root_folder, 'interpolations')
release_folder = os.path.join(font_root_folder, 'release')

class InterpolationTarget:
    def __init__(self, target_size, target_weight, is_italic, base_a, base_b, proportion, is_mock = False):
        self.target_size_code = target_size
        self.target_weight_code = target_weight
        self.is_italic = is_italic
        self.size_style_id = target_size
        self.target_short_name = self.get_target_short_name(target_size, target_weight, is_italic)
        self.target_path = os.path.join(interpolations_folder, f'{self.target_short_name}.sfdir')
        self.target_long_name = self.get_target_long_name(target_size, target_weight, is_italic)
        self.base_a_path = base_a.target_path if isinstance(base_a, InterpolationTarget) else base_a
        self.base_b_path = base_b.target_path if isinstance(base_b, InterpolationTarget) else base_b
        self.proportion = proportion
        self.os2_weight_code = self.get_os2_weight_code(target_weight)
        self.family = 'Coelacanth'
        self.weight_name = self.get_weight_name(target_weight)
        self.style_name = self.get_style_name(target_weight, is_italic)
        self.size_points_and_range = self.get_size_points_and_range(target_size)
        self.style_id = target_weight + 1 + (len(self.target_weights) if is_italic else 0)
        self.is_mock = is_mock
        
    def to_dict(self):
        """Convert the InterpolationTarget instance to a dictionary for serialization."""
        return {
            'target_size_code': self.target_size_code,
            'target_weight_code': self.target_weight_code,
            'is_italic': self.is_italic,
            'base_a_path': self.base_a_path,
            'base_b_path': self.base_b_path,
            'proportion': self.proportion,
            'is_mock': self.is_mock,
        }

    @classmethod
    def from_dict(cls, data):
        """Create an InterpolationTarget instance from a dictionary."""
        return cls(
            target_size=data['target_size_code'],
            target_weight=data['target_weight_code'],
            is_italic=data['is_italic'],
            base_a=data['base_a_path'],
            base_b=data['base_b_path'],
            proportion=data['proportion'],
            is_mock=data['is_mock'],
        )

    roman_bases = [
        "CoelacanthDisplayHairline.sfdir", "CoelacanthDisplay.sfdir", "CoelacanthDisplayGross.sfdir",
        "CoelacanthHairline.sfdir", "Coelacanth.sfdir", "CoelacanthGross.sfdir",
        "CoelacanthPearlHairline.sfdir", "CoelacanthPearl.sfdir", "CoelacanthPearlGross.sfdir"
    ]
    italic_bases = [
        "CoelacanthItalicHairline.sfdir", "CoelacanthItalic.sfdir", "CoelacanthItalicGross.sfdir",
        "CoelacanthItalicHairline.sfdir", "CoelacanthItalic.sfdir", "CoelacanthItalicGross.sfdir",
        "CoelacanthItalicPearlHairline.sfdir", "CoelacanthItalicPearl.sfdir", "CoelacanthItalicPearlGross.sfdir"
    ]
    target_sizes = [ "Display", "Subhd", "", "Caption", "Subcapt", "Pearl" ]
    target_sizes_long_names = [ "Display", "Subheading", "", "Caption", "Subcaption", "Pearl" ]
    target_size_points_and_ranges = [ [60, 33, 72], [24, 17.3, 32.9], [14, 11, 17.2], [9, 7, 10.9], [6, 5.6, 6.9], [5, 4, 5.5] ]
    target_weights = [ "ExtraLt", "Lt", "", "Semibd", "Bold", "Heavy" ]
    target_weights_long_names = [ "ExtraLight", "Light", "", "Semibold", "Bold", "Heavy" ]
    target_weights_os2_codes = [ "200", "300", "400", "600", "700", "800" ]

    @classmethod
    def get_target_short_name(cls, target_size, target_weight, is_italic):
        return f"Coelacanth{cls.target_sizes[target_size]}{cls.target_weights[target_weight]}{'It' if is_italic else ''}"

    @classmethod
    def get_target_long_name(cls, target_size, target_weight, is_italic):
        return f"Coelacanth{' Italic' if is_italic else ''}{' ' + cls.target_sizes_long_names[target_size] if cls.target_sizes_long_names[target_size] else ''}{' ' + cls.target_weights_long_names[target_weight] if cls.target_weights_long_names[target_weight] else ''}".strip()

    @classmethod
    def get_os2_weight_code(cls, target_weight):
        return cls.target_weights_os2_codes[target_weight]

    @classmethod
    def get_weight_name(cls, target_weight):
        return "Regular" if cls.target_weights_long_names[target_weight] == "" else cls.target_weights_long_names[target_weight]

    @classmethod
    def get_style_name(cls, target_weight, is_italic):
        return f"{cls.target_weights_long_names[target_weight]}Italic" if is_italic else cls.target_weights_long_names[target_weight]

    @classmethod
    def get_size_points_and_range(cls, target_size):
        return cls.target_size_points_and_ranges[target_size]

    @classmethod
    def get_roman_base(cls, font_index):
        return os.path.join(font_root_folder, cls.roman_bases[font_index])

    @classmethod
    def get_italic_base(cls, font_index):
        return os.path.join(font_root_folder, cls.italic_bases[font_index])

    @classmethod
    def get_all_targets(cls):
        targets = [
            InterpolationTarget(0, 0, False, cls.get_roman_base(1), cls.get_roman_base(0), 0.393),
            InterpolationTarget(0, 1, False, cls.get_roman_base(1), cls.get_roman_base(0), 0.216),
            InterpolationTarget(0, 2, False, cls.get_roman_base(1), cls.get_roman_base(1), 0.0),
            InterpolationTarget(0, 3, False, cls.get_roman_base(1), cls.get_roman_base(2), 0.266),
            InterpolationTarget(0, 4, False, cls.get_roman_base(1), cls.get_roman_base(2), 0.590),
            InterpolationTarget(0, 5, False, cls.get_roman_base(2), cls.get_roman_base(2), 0.0),
            InterpolationTarget(2, 0, False, cls.get_roman_base(4), cls.get_roman_base(3), 0.477),
            InterpolationTarget(2, 1, False, cls.get_roman_base(4), cls.get_roman_base(3), 0.261),
            InterpolationTarget(2, 2, False, cls.get_roman_base(4), cls.get_roman_base(4), 0.0),
            InterpolationTarget(2, 3, False, cls.get_roman_base(4), cls.get_roman_base(5), 0.312),
            InterpolationTarget(2, 4, False, cls.get_roman_base(4), cls.get_roman_base(5), 0.689),
            InterpolationTarget(2, 5, False, cls.get_roman_base(4), cls.get_roman_base(5), 1.147),
            InterpolationTarget(5, 0, False, cls.get_roman_base(7), cls.get_roman_base(6), 0.957),
            InterpolationTarget(5, 1, False, cls.get_roman_base(7), cls.get_roman_base(6), 0.692),
            InterpolationTarget(5, 2, False, cls.get_roman_base(7), cls.get_roman_base(6), 0.370),
            InterpolationTarget(5, 3, False, cls.get_roman_base(7), cls.get_roman_base(8), 0.020),
            InterpolationTarget(5, 4, False, cls.get_roman_base(7), cls.get_roman_base(8), 0.510),
            InterpolationTarget(5, 5, False, cls.get_roman_base(7), cls.get_roman_base(8), 1.103),
            # Italics
            InterpolationTarget(0, 0, True, cls.get_italic_base(1), cls.get_italic_base(0), 0.393),
            InterpolationTarget(0, 1, True, cls.get_italic_base(1), cls.get_italic_base(0), 0.216),
            InterpolationTarget(0, 2, True, cls.get_italic_base(1), cls.get_italic_base(1), 0.0),
            InterpolationTarget(0, 3, True, cls.get_italic_base(1), cls.get_italic_base(2), 0.266),
            InterpolationTarget(0, 4, True, cls.get_italic_base(1), cls.get_italic_base(2), 0.590),
            InterpolationTarget(0, 5, True, cls.get_italic_base(2), cls.get_italic_base(2), 0.0),
            InterpolationTarget(2, 0, True, cls.get_italic_base(4), cls.get_italic_base(3), 0.477),
            InterpolationTarget(2, 1, True, cls.get_italic_base(4), cls.get_italic_base(3), 0.261),
            InterpolationTarget(2, 2, True, cls.get_italic_base(4), cls.get_italic_base(4), 0.0),
            InterpolationTarget(2, 3, True, cls.get_italic_base(4), cls.get_italic_base(5), 0.312),
            InterpolationTarget(2, 4, True, cls.get_italic_base(4), cls.get_italic_base(5), 0.689),
            InterpolationTarget(2, 5, True, cls.get_italic_base(4), cls.get_italic_base(5), 1.147),
            InterpolationTarget(5, 0, True, cls.get_italic_base(7), cls.get_italic_base(6), 0.957),
            InterpolationTarget(5, 1, True, cls.get_italic_base(7), cls.get_italic_base(6), 0.692),
            InterpolationTarget(5, 2, True, cls.get_italic_base(7), cls.get_italic_base(6), 0.370),
            InterpolationTarget(5, 3, True, cls.get_italic_base(7), cls.get_italic_base(8), 0.020),
            InterpolationTarget(5, 4, True, cls.get_italic_base(7), cls.get_italic_base(8), 0.510),
            InterpolationTarget(5, 5, True, cls.get_italic_base(7), cls.get_italic_base(8), 1.103),
        ]
        second_interpolation_targets = []
        for i in range(6):
            second_interpolation_targets.append(InterpolationTarget(1, i, False, targets[i + 6], targets[i], 0.543))
            second_interpolation_targets.append(InterpolationTarget(3, i, False, targets[i + 6], targets[i + 12], 0.309))
            second_interpolation_targets.append(InterpolationTarget(4, i, False, targets[i + 6], targets[i + 12], 0.741))
            # Italics
            second_interpolation_targets.append(InterpolationTarget(1, i, True, targets[i + 24], targets[i + 18], 0.543))
            second_interpolation_targets.append(InterpolationTarget(3, i, True, targets[i + 24], targets[i + 30], 0.309))
            second_interpolation_targets.append(InterpolationTarget(4, i, True, targets[i + 24], targets[i + 30], 0.741))
        return targets + second_interpolation_targets

    @classmethod
    def get_mock_targets(cls):
        mockTargets = [
            InterpolationTarget(2, 4, False, cls.get_roman_base(4), cls.get_roman_base(5), 0.5, True),
            InterpolationTarget(2, 1, False, cls.get_roman_base(4), cls.get_roman_base(3), 0.5, True),
            InterpolationTarget(5, 2, False, cls.get_roman_base(4), cls.get_roman_base(7), 0.5, True),
            InterpolationTarget(0, 2, False, cls.get_roman_base(4), cls.get_roman_base(1), 0.5, True),
            InterpolationTarget(4, 4, False, cls.get_roman_base(4), cls.get_roman_base(8), 0.5, True),
            InterpolationTarget(4, 1, False, cls.get_roman_base(4), cls.get_roman_base(6), 0.5, True),
            InterpolationTarget(1, 4, False, cls.get_roman_base(4), cls.get_roman_base(2), 0.5, True),
            InterpolationTarget(1, 1, False, cls.get_roman_base(4), cls.get_roman_base(0), 0.5, True),
            InterpolationTarget(2, 4, True, cls.get_italic_base(4), cls.get_italic_base(5), 0.5, True),
            InterpolationTarget(2, 1, True, cls.get_italic_base(4), cls.get_italic_base(3), 0.5, True),
            InterpolationTarget(5, 2, True, cls.get_italic_base(4), cls.get_italic_base(7), 0.5, True),
            # InterpolationTarget(0, 2, True, cls.get_italic_base(4), cls.get_italic_base(1), 0.5, True),
            InterpolationTarget(4, 4, True, cls.get_italic_base(4), cls.get_italic_base(8), 0.5, True),
            InterpolationTarget(4, 1, True, cls.get_italic_base(4), cls.get_italic_base(6), 0.5, True),
            # InterpolationTarget(1, 4, True, cls.get_italic_base(4), cls.get_italic_base(2), 0.5, True),
            # InterpolationTarget(1, 1, True, cls.get_italic_base(4), cls.get_italic_base(0), 0.5, True),
        ]
        return mockTargets

    @classmethod
    def get_all_base_font_filenames(cls):
        return [cls.get_roman_base(i) for i in range(len(cls.roman_bases))]

    @staticmethod
    def get_regular_weight_filename_for_optical_size_font(font_filename):
        return font_filename.replace("Hairline.sfd", ".sfd").replace("Gross.sfd", ".sfd")

    @classmethod
    def _init_mock(base_a_path, base_b_path):
        result = InterpolationTarget(
            target_size=2,
            target_weight=2,
            is_italic=False, 
            base_a_path=base_a_path, 
            base_b_path=base_b_path, 
            proportion=0.5
        )
        result.target_short_name = result.target_long_name = "Mock"
        result.target_path = os.path.join(interpolations_folder, f'{result.target_short_name}.sfdir')
        return result