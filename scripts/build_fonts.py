import fontforge
import os
import glob
import zipfile
import json
import subprocess
import argparse
from interpolation_target import InterpolationTarget, interpolations_folder, release_folder
from notices import font_version

def copy_missing_anchors(from_font_filename, to_font_filename):
    from_font = fontforge.open(from_font_filename)
    to_font = fontforge.open(to_font_filename)
    
    print(f"Checking for missing anchors in {os.path.basename(to_font_filename)}")
    has_changed = False

    for glyph_name in from_font:
        if glyph_name not in to_font:
            continue

        from_glyph = from_font[glyph_name]
        to_glyph = to_font[glyph_name]

        # Remove anchors in to_glyph that are not in from_glyph
        to_glyph_anchor_names = {anchor[0] for anchor in to_glyph.anchorPoints}
        from_glyph_anchor_names = {anchor[0] for anchor in from_glyph.anchorPoints}

        new_anchor_points = []
        for anchor in to_glyph.anchorPoints:
            if anchor[0] in from_glyph_anchor_names:
                new_anchor_points.append(anchor)
            else:
                print(f"Removed {anchor[0]} from {glyph_name}")
                has_changed = True

        to_glyph.anchorPoints = new_anchor_points

        # Add anchors from from_glyph to to_glyph that are missing
        for anchor in from_glyph.anchorPoints:
            anchor_name, anchor_type, x, y = anchor
            if anchor_name not in to_glyph_anchor_names:
                to_glyph.addAnchorPoint(anchor_name, anchor_type, x, y)
                print(f"Added {anchor_name} to {glyph_name}")
                has_changed = True

    if has_changed:
        to_font.save(to_font_filename.replace(".sfdir", ".newnew.sfd"))

def zip_release():
    # Paths
    zip_filename = f"coelacanth_v{font_version}.zip"
    zip_filepath = os.path.join(release_folder, zip_filename)
    os.makedirs(release_folder, exist_ok=True)
    otf_files = glob.glob(os.path.join(interpolations_folder, "*.otf"))
    with zipfile.ZipFile(zip_filepath, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for otf_file in otf_files:
            arcname = os.path.basename(otf_file)  # Store only the filename in the zip
            zipf.write(otf_file, arcname)
            print(f"Added to zip: {otf_file}")
    print(f"Created release file: {zip_filepath}")

def main():
    parser = argparse.ArgumentParser(description="Interpolation App")
    parser.add_argument('--test', action='store_true', help='Use test targets')
    args = parser.parse_args()

    if args.test:
        targets = InterpolationTarget.get_mock_targets()
    else:
        targets = InterpolationTarget.get_all_targets()

    if not args.test:
        for font_filename in InterpolationTarget.get_all_base_font_filenames():
            base_weight_filename = InterpolationTarget.get_regular_weight_filename_for_optical_size_font(font_filename)
            if base_weight_filename == font_filename:
                continue
            copy_missing_anchors(base_weight_filename, font_filename)
        # Delete any pre-existing interpolated OTF files
        otf_files = glob.glob(os.path.join(interpolations_folder, "*.otf"))
        for otf_file in otf_files:
            try:
                os.remove(otf_file)
                print(f"Deleted OTF file: {otf_file}")
            except Exception as e:
                print(f"Failed to delete {otf_file}: {e}")

    for target in targets:
        target_data = json.dumps(target.to_dict())
        # Isolate each font interpolation in its own subprocess, to improve stability
        # by avoiding any residual state or memory leaks from previous interpolations.
        # FontForge is not very stable!
        subprocess.run(['python3', 'scripts/interpolate_font.py', target_data], check=True)
    
    if not args.test:
        zip_release()


if __name__ == "__main__":
    main()
